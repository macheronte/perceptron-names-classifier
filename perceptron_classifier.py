vowels = ['a', 'e', 'i', 'o', 'u', 'à', 'è', 'ì', 'ò', 'ù', 'é']

def calculate_features(name):
    name = name.lower()
    lenght = len(name)
    
    n_vowels = 0
    n_consonants = 0

    end_a = 0
    end_e = 0
    end_i = 0
    end_o = 0
    end_u = 0

    for i in name:
        if(i in vowels):
            n_vowels += 1
        else:
            n_consonants += 1

    last_letter = name[-1].lower()

    if last_letter == 'a' or last_letter == 'à':
        end_a = 1
    elif last_letter == 'e' or last_letter == 'è' or last_letter == 'é':
        end_e = 1
    elif last_letter == 'i' or last_letter == 'ì':
        end_i = 1
    elif last_letter == 'o' or last_letter == 'ò':
        end_o = 1
    elif last_letter == 'u' or last_letter == 'ù':
        end_u = 1

    return lenght, n_vowels, n_consonants, end_a, end_e, end_i, end_o, end_u

import pandas as pd
import numpy as np

def load_dataset(name, test_size=0.3):
    df = pd.read_csv(name, header=None)

    y = df.iloc[1:,-1].values
    X = df.iloc[1:,1:-1].values

    y = np.where(y == '1', 1, -1)

    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state = 2, stratify=y)

    return X_train, X_test, y_train, y_test

import matplotlib.pyplot as plt


X_train, X_test, y_train, y_test = load_dataset('dataset_names.txt')

from sklearn.linear_model import Perceptron

prc = Perceptron(max_iter=40)
prc.fit(X_train, y_train)

print('Score: ' + str(prc.score(X_test, y_test)))
print('Weights: ' + str(prc.coef_))

while True:
    print(prc.predict(np.asarray(calculate_features(input('Enter a word: '))).reshape(1,-1)))
